#  Diagrama de contexto

![diagram](https://kroki.io/c4plantuml/svg/eNp9U8ttwzAMvXsK1qcECHrqAHFzbdE2GSBgZMZQYUuCPkHTtTpCFytlOZHjGjmJIB_feyTttfNofeja4kEq0YaaYPO032jl6cs_mlgovPQtwe7sPHUwlKCW2Fjs4KgtVCdSPliqjCmKd7JOq4XpH1xBmRJYcvgRJCk4BCcQUHh5kjXW5OAMJhBLW3JkT2jLZZHkFj12P8Iyy3PfX-VcmVQ66SmR2wm7JWzlN6cHAVcuAa5WsSElZLRapShb1UdLgsZs2VqqTby9_W8YmZOqYQe39lYgtHKh5Tu0GM3qgydFlsG82w6F_P1RwMuJ1ZH_q32njbaeWGaXoqi4I-BR0DYIhpuYHGKnFFID9lScIn6YeCCI5IaEvwCy1mXiITMZeZuy05lfUdHnLVMUu1klD7GlNn8rc9cOjtgBw_bbfKnZ3WfkS17KvOcEHbAzqvf55zru6cSWOca73opiTarmH_APnlVKUg==)

## codigo

```
@startuml
!include C4_Context.puml

title System Context diagram for AventureApp

Person(persona, "Persona", "Quien busca actividades y puede reservar")
System(busca_actividades, "Busca Actividades", "Permite buscar actividades y realizar reservas")  

Person(agencia, "Agencia", "Quien ofrece actividades")
System(ofrece_actividades, "Ofrece actividades", "Permite ingresar actividades, consultarlas y obtener información de las reservas") 

Person(soporte, "Soporte", "Se encarga prestar servicio a las tareas de soporte respecto a las reservas")
System(reserva_actividades, "Reserva actividades", "Maneja las reservas de actividades") 

Rel(persona, busca_actividades, "use")
Rel(agencia, ofrece_actividades, "use")
Rel(soporte, reserva_actividades, "use")

Rel(busca_actividades, ofrece_actividades, "use")
Rel(busca_actividades, reserva_actividades, "use")
Rel(ofrece_actividades, reserva_actividades, "use")

@enduml
```

