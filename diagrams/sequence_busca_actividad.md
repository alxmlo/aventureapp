# Diagrama secuencia busca actividad

[![diagram](https://mermaid.ink/img/eyJjb2RlIjoic2VxdWVuY2VEaWFncmFtXG4gICAgXG4gICAgYWN0b3IgUGVyc29uXG4gICAgcGFydGljaXBhbnQgUFggYXMgUHJveHlcbiAgICBwYXJ0aWNpcGFudCBXQSBhcyBXZWIgQXBsaWNhdGlvblxuICAgIHBhcnRpY2lwYW50IENBIGFzIENhY2hlXG4gICAgcGFydGljaXBhbnQgT0EgYXMgT2ZyZWNlIEFjdGl2aWRhZGVzXG5cbiAgICBQZXJzb24tPj5QWDogQnVzY2EgQWN0aXZpZGFkXG4gICAgUFgtPj5XQTogUmVkaXJpZ2UgQ29uc3VsdGFcbiAgICBhY3RpdmF0ZSBXQVxuICAgIGFsdCBCdXNjYSBlbiBDYWNoZVxuICAgICAgICBXQS0-PkNBOiBFbnZpYSBDb25zdWx0YVxuICAgICAgICBDQS0tPj5XQTogIFJlc3VsdGFkb3NcbiAgICBlbHNlXG4gICAgICAgIFdBLT4-T0E6IEVudmlhIENvbnN1bHRhXG4gICAgICAgIE9BLS0-PldBOiAgUmVzdWx0YWRvc1xuICAgIGVuZFxuICAgIFdBLS0-PlBlcnNvbjogTGlzdGFkbyBkZSBBY3RpdmlkYWRlc1xuICAgIGRlYWN0aXZhdGUgV0EiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlLCJhdXRvU3luYyI6dHJ1ZSwidXBkYXRlRGlhZ3JhbSI6ZmFsc2V9)](https://mermaid.live/edit/#eyJjb2RlIjoic2VxdWVuY2VEaWFncmFtXG4gICAgXG4gICAgYWN0b3IgUGVyc29uXG4gICAgcGFydGljaXBhbnQgUFggYXMgUHJveHlcbiAgICBwYXJ0aWNpcGFudCBXQSBhcyBXZWIgQXBsaWNhdGlvblxuICAgIHBhcnRpY2lwYW50IENBIGFzIENhY2hlXG4gICAgcGFydGljaXBhbnQgT0EgYXMgT2ZyZWNlIEFjdGl2aWRhZGVzXG5cbiAgICBQZXJzb24tPj5QWDogQnVzY2EgQWN0aXZpZGFkXG4gICAgUFgtPj5XQTogUmVkaXJpZ2UgQ29uc3VsdGFcbiAgICBhY3RpdmF0ZSBXQVxuICAgIGFsdCBCdXNjYSBlbiBDYWNoZVxuICAgICAgICBXQS0-PkNBOiBFbnZpYSBDb25zdWx0YVxuICAgICAgICBDQS0tPj5XQTogIFJlc3VsdGFkb3NcbiAgICBlbHNlXG4gICAgICAgIFdBLT4-T0E6IEVudmlhIENvbnN1bHRhXG4gICAgICAgIE9BLS0-PldBOiAgUmVzdWx0YWRvc1xuICAgIGVuZFxuICAgIFdBLS0-PlBlcnNvbjogTGlzdGFkbyBkZSBBY3RpdmlkYWRlc1xuICAgIGRlYWN0aXZhdGUgV0EiLCJtZXJtYWlkIjoie1xuICBcInRoZW1lXCI6IFwiZGVmYXVsdFwiXG59IiwidXBkYXRlRWRpdG9yIjpmYWxzZSwiYXV0b1N5bmMiOnRydWUsInVwZGF0ZURpYWdyYW0iOmZhbHNlfQ)

## codigo

```
sequenceDiagram
    
    actor Person
    participant PX as Proxy
    participant WA as Web Aplication
    participant CA as Cache
    participant OA as Ofrece Actividades

    Person->>PX: Busca Actividad
    PX->>WA: Redirige Consulta
    activate WA
    alt Busca en Cache
        WA->>CA: Envia Consulta
        CA-->>WA:  Resultados
    else
        WA->>OA: Envia Consulta
        OA-->>WA:  Resultados
    end
    WA-->>Person: Listado de Actividades
    deactivate WA
```
